# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Sainsbury's assessment
* Version 0.3 (adds tests)

### How do I get set up? ###

* Tested on Windows 7 64 bit with latest vagrant
* Install vagrant - https://www.vagrantup.com/, check out this repository - if you don't have rsync installed you may have to comment this out in the downloaded machine image - see https://github.com/mitchellh/vagrant/issues/6696 
* Switch to the checked out directory and run "vagrant up"
* Vagrant should pull down VirtualBox and the vm image to use, then build the servers, deploy the packages and application, then test for you
* If you change the code to see what happens you'll need to run "vagrant provision"

### Who do I talk to? ###

* Sean Turner - seanturner83@gmail.com

### What could be improved? ###

This is a very lightweight solution and only of limited use for re-purposing. In reality the code would be in a repository of its own, and a CI pipeline would handle checking out, building and testing changes before deploying the app only to the box(es). A different vagrant driver could be used (e.g. docker or a cloud API one). The tests could be implemented in something more sophisticated than bash.