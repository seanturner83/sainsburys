#
# Cookbook Name:: test
# Recipe:: default
#
# Copyright (c) 2016 Sean Turner, All Rights Reserved.

bash "test" do
  code <<-EOH
    a=0
    b=0
    if curl -s http://192.168.33.10 | grep goserver001; then ((a++)); fi
    if curl -s http://192.168.33.10 | grep goserver001; then ((a++)); fi
    if curl -s http://192.168.33.10 | grep goserver002; then ((b++)); fi
    if curl -s http://192.168.33.10 | grep goserver002; then ((b++)); fi
    if (( a==1 )) && (( b==1 )); then echo -e '\033[32mTest commands ran successfully! \033[0m'; exit 0
      else echo -e '\033[31mTest failed!!! \033[0m'; exit 1
    fi
  EOH
end