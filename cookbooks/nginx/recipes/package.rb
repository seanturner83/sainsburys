#
# Cookbook Name:: nginx
# Recipe:: package
#
# Copyright 2016, Sean Turner
#
# All rights reserved - Do Not Redistribute
#
package "nginx"
service "nginx" do
  action :enable
end