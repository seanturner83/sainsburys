#
# Cookbook Name:: nginx
# Recipe:: config
#
# Copyright 2016, Sean Turner
#
# All rights reserved - Do Not Redistribute
#

template '/etc/nginx/conf.d/balancer.conf' do
  source 'balancer.conf.erb'
  owner 'root'
  group 'root'
  mode '0644'
  action :create

  notifies :restart, 'service[nginx]', :immediately

end