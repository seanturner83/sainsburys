#
# Cookbook Name:: go
# Recipe:: compile
#
# Copyright (c) 2016 Sean Turner, All Rights Reserved.

package "psmisc"

bash "compile_go_app" do
  cwd ::File.dirname("/opt/go/")
  user "root"
  code "runuser -l go -s /bin/bash -c 'go build /opt/go/server.go'"
  creates "/opt/go/server"
  notifies :run, "bash[remove_installed_go_app]", :immediately
end

bash "remove_installed_go_app" do
  user "root"
  code "killall goserver1; rm -f /opt/go/app/goserver1"
  action :nothing
end

bash "install_go_app" do
  user "root"
  code "killall goserver1; cp /opt/go/server /opt/go/app/goserver1; chown root:go /opt/go/app/goserver1"
  creates "/opt/go/app/goserver1"
end