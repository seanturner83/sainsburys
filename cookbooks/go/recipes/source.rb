#
# Cookbook Name:: go
# Recipe:: source
#
# Copyright (c) 2016 Sean Turner, All Rights Reserved.

template '/opt/go/server.go' do
  source 'server.go.erb'
  owner 'go'
  group 'go'
  mode '0444'
  action :create
  notifies :run, "bash[remove_go_app]", :immediately
end

bash "remove_go_app" do
  user "root"
  code "rm -f /opt/go/server"
  action :nothing
end