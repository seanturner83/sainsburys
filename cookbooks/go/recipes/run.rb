#
# Cookbook Name:: go
# Recipe:: run
#
# Copyright (c) 2016 Sean Turner, All Rights Reserved.

directory "/var/log/go/server" do
  user "go"
  group "go"
  mode "0755"
  action :create
  recursive true
end

execute "run_go_app" do
  cwd ::File.dirname("/opt/go/app/")
  user "root"
  command "runuser -l go -s /bin/bash -c '/opt/go/app/goserver1 & >> /var/log/go/server/out.log 2>&1'"
  not_if "ps -ef | grep -v grep | grep goserver1"
end