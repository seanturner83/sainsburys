#
# Cookbook Name:: go
# Recipe:: environment
#
# Copyright (c) 2016 Sean Turner, All Rights Reserved.

user "go" do
  comment "Go User"
  uid "1001"
  home "/opt/go"
  shell "/sbin/nologin"
end

directory "/opt/go" do
  owner "go"
  group "go"
  mode "0750"
  action :create
end

directory "/opt/go/app" do
  owner "root"
  group "go"
  mode "0550"
  action :create
end