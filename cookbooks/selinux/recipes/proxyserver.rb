#
# Cookbook Name:: selinux
# Recipe:: default
#
# Copyright (c) 2016 Sean Turner, All Rights Reserved.

execute "httpd_can_network_connect" do
  command "setsebool httpd_can_network_connect 1"
  not_if "getsebool httpd_can_network_connect | egrep on$"
end